/* Modules */
const mongoose = require("mongoose")
/* Conection db */
mongoose.connect(process.env.URL,{ useUnifiedTopology: true,useNewUrlParser: true , useCreateIndex:true})
module.exports = mongoose
