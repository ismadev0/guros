/* Debug */
const debug = require('debug')('mutation:models:Log.js')

/* Conection db*/
var mongoose = require('../../config/db')

const Schema = mongoose.Schema
const LogSchema = new Schema({
        id: {type: Schema.Types.ObjectId},
        idTrace: {type: String},
        time: {type: Number},
        method: {type: String},
        endpoint: {type: String},
        body: {type: String},
        query: {type: String},
        ip: {type: String},
        token: {type: String},
        statusCode: {type: Number},
        timeProcess: {type: String},
        responseBody: {type: String}
    }
)
const LogModel = mongoose.model('Logs', LogSchema);
module.exports = LogModel
