/* Debug */
const debug = require('debug')('mutation:models:Users.js')
/* Modules */
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

/* Conection db */
var mongoose = require('../../config/db')

const Schema = mongoose.Schema
const userSchema = new Schema(
    { 
        id: {type: Schema.Types.ObjectId},
        name: {type: String,required: true},
        email: {type: String,unique: true,required: true},
        password: {type: String,required: true},
        tokens: [{
            token: {
                type: String,
                required: true
            }
        }]
    }
)
userSchema.pre('save', async function (next){
    debug('Hash the password before saving the user model')
    const user = this
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }
    next()
})
userSchema.statics.generateAuthToken = async function (user) {
    debug('Generate an auth token for the user')
    //const user = this
    const token = jwt.sign({_id: user._id}, process.env.JWT_PASSWORD, { expiresIn: '24h' })
    user.tokens = user.tokens.concat({token})
    //user.token = token
    await user.save()
    return token
}
userSchema.statics.findByCredentials = async (email, password) => {
    debug('Search for a user by email and password.')
    const user = await UserModel.findOne({ email} )
    if (!user) {
        return user
    }
    const isPasswordMatch = await bcrypt.compare(password, user.password)
    if (!isPasswordMatch) 
        return false
    return user
}
const UserModel = mongoose.model('Users', userSchema);
module.exports = UserModel
