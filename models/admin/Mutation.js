/* Conection db */
var mongoose = require('../../config/db')

const Schema = mongoose.Schema
const mutationSchema = new Schema(
    { 
        id: {type: Schema.Types.ObjectId},
        dna: {type: Array,required: true},
        hasMutation: {type: Boolean}
    }
)
const MutationModel = mongoose.model('Mutation', mutationSchema);
module.exports = MutationModel
