/* Debug */
const SOURCE = 'mutation:routes:users'
const debug = require('debug')(SOURCE)
/* Modules */
const express = require('express')
const _ = require('lodash')
/* App */
const router = express.Router()
/* Controllers */
const controllerUsers = require('../controllers/users/indexController')
const controllerPolicies = require('../controllers/public/PoliciesController')

const routes = {
	'/': {
		GET: {
			middleware: [
				controllerPolicies.checkApiAuthorization,
				controllerPolicies.checkAuthJwt,
				controllerUsers.getAllUsers
			]
		},
		POST: {
			middleware: [
				controllerPolicies.checkApiAuthorization,
				controllerUsers.registerUserCheckParameters,
				controllerUsers.registerUser
			]
		}
	},
	'/login': {
		POST: {
			middleware: [
				controllerPolicies.checkApiAuthorization,
				controllerUsers.loginCheckParameters,
				controllerUsers.login
			]
		}
	}
}
_.forOwn(routes, (methods, endpoint) => {
	_.forOwn(methods, (details, method) => {
		method = method.toLowerCase()
		if (typeof router[method] === 'function') {
			debug('Loanding endpoint %s#%s', endpoint, method)
			router[method](endpoint, details.middleware)
		}
	})
})

module.exports = router
