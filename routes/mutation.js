/* Debug */
const SOURCE = 'mutation:routes:mutation'
const debug = require('debug')(SOURCE)
/* Modules */
const express = require('express')
const _ = require('lodash')
/* App */
const router = express.Router()
/* Controllers */
const controllerMutation = require('../controllers/mutation/indexController')
const controllerPolicies = require('../controllers/public/PoliciesController')

const routes = {
	'/': {
		POST: {
			middleware: [
				controllerPolicies.checkApiAuthorization,
				//controllerPolicies.checkAuthJwt,
				controllerMutation.validateParams,
				controllerMutation.verifyDna,
				controllerMutation.registerDB,
				controllerMutation.response
			]
		}
	},
	'/stats': {
		GET: {
			middleware: [
				controllerPolicies.checkApiAuthorization,
				//controllerPolicies.checkAuthJwt,
				controllerMutation.getStats
			]
		}
	}
}
_.forOwn(routes, (methods, endpoint) => {
	_.forOwn(methods, (details, method) => {
		method = method.toLowerCase()
		if (typeof router[method] === 'function') {
			debug('Loanding endpoint %s#%s', endpoint, method)
			router[method](endpoint, details.middleware)
		}
	})
})

module.exports = router
