
# Guros Challenge!

Instructions on PDF

## Requirements
-   Node >= 12
-   mongo > 5

## Installation
**Clone**  this repository
```
git https://gitlab.com/ismadev0/guros.git
```
Move to  **root folder**  and run:
```
npm install
```
Set the variables in your local environment:
see inside .env file
```
URL=mongodb://127.0.0.1:27017/devdb
#URL=mongodb://127.0.0.1:27017/test
API_KEY=c3VwZXJhZG1pbjp1QHR3XkVXQ3BVWDFrdEN0
JWT_PASSWORD=mysuperpass1
NODE_ENV=development
DEBUG=mutation*
PORT=3000
```
Now let's start our application run:
```
npm start
```
## Postman Collection
```
https://gitlab.com/ismadev0/guros/-/blob/main/docs/guros.postman_collection.json
```
## Integration Testing
Environment Variables:
```
URL=mongodb://127.0.0.1:27017/test
API_KEY=c3VwZXJhZG1pbjp1QHR3XkVXQ3BVWDFrdEN0
DEBUG=mutation*
PORT=3000
```
Execute
```
npm run test
```
You will see something like:
![image](https://user-images.githubusercontent.com/9513390/158911886-c085f536-2053-4172-9b20-02dc0a9a0cfe.png)

## Demo API in EC2 AWS

**FYI:**
This API has **API_KEY** *(check local environment)* for secutiry and **JWT** for auth user

1. Create a new user: POST /users
2. Login the user to get your token: POST /users/login
3. For all request you will need **Authentication** *(JWT)* & **Authorization** *(API_KEY)* in the headers

**Domain:**
http://ec2-18-217-15-76.us-east-2.compute.amazonaws.com:3000/

**Note:** If you want test this api, use it instead of localhost:3000 *(in the postman collection)*

**Cloud:**

 - mongo cloud 
 - aws EC2
