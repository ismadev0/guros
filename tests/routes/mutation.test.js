const supertest = require('supertest')
const app = require('../../app')


describe("/POST mutation", () => {
	test("Should be 403 - Fail Authorization key", done => {
		supertest(app)
		.post("/mutation")
		.set({
			"Authorization": "XXXXXXXX",
			"Accept":"application/json"
		})
		.then(response => {
			expect(response.statusCode).toBe(403)
			done()
		})
	})
	test("Should be 422 - Missing parameters", done => {
		supertest(app)
		.post("/mutation")
		.set({
			"Authorization": "Bearer "+process.env.API_KEY,
			"Accept":"application/json"
		})
		.then(response => {
		expect(response.statusCode).toBe(422)
		expect(response.body).toEqual(
			{
				"msg": [
					{
						"msg": "dna can not be an empty value",
						"param": "dna",
						"location": "body"
					},
					{
						"msg": "dna need be an array value",
						"param": "dna",
						"location": "body"
					}
				]
			}
		)
		done()
		})
	})
	test("Should be 200 - Verify DNA", done => {
		supertest(app)
		.post("/mutation")
		.send({
			"dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCACTA", "TCACTG"]
		})
		.set({
			"Authorization": "Bearer "+process.env.API_KEY,
			"Accept":"application/json"
		})
		.then(response => {
		expect(response.statusCode).toBe(200)
		done()
		})
	})
	test("Should be 403 - Verify DNA", done => {
		supertest(app)
		.post("/mutation")
		.send({
			"dna":["TTGCGA", "CAGTGC", "TTATGT", "AGGAAG", "CCACTA", "TAACTG"]
		})
		.set({
			"Authorization": "Bearer "+process.env.API_KEY,
			"Accept":"application/json"
		})
		.then(response => {
		expect(response.statusCode).toBe(403)
		done()
		})
	})
})

