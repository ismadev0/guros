/* Debug */
const SOURCE = 'mutation:controllers:public:policies.js'
const debug = require('debug')(SOURCE)

/* Models */
const UserModel = require('../../models/admin/User')

/* Modules */
const jwt = require('jsonwebtoken')


module.exports = {
	checkApiAuthorization: (req, res, next) => {
        try {
            debug("Check authorization")
            debug('authorization: '+req.headers.authorization)
            let apiKey = req.headers.authorization
            if (!apiKey || apiKey != ("Bearer " + process.env.API_KEY)) {
                res.status(403).send({msg: 'Invalid authorization'})
                return
            }
            debug("Authorized!")
            next()
        } catch (error) {
            next(error)
        }
    },
    checkAuthJwt: async (req,res,next) => {
        try {
            debug("Check token: "+req.headers.authentication)
            const token = req.header('Authentication')
            const data = jwt.verify(token, process.env.JWT_PASSWORD, (err, data) => {
                if (err) {
                    debug("Error token: " + err.message)
                    return false
                }
                debug('token authorizated!')
                return data
            })
            if(!data){
                res.status(401).send({msg: 'Invalid token'})
                return
            }
            const user = await UserModel.findOne({ _id: data._id, 'tokens.token': token })
            if (!user) {
                res.status(401).send({msg: 'Invalid token'})
                return
            }
            req.user = user
            req.token = token
            debug("Authorized!")
            next()
        } catch (error) {
            next(error)
        }
    }
}