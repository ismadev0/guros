/* Debug */
const SOURCE = 'mutation:controllers:index.js'
const debug = require('debug')(SOURCE)

module.exports = {
	homePage: (req, res, next) => {
        try {
            debug("home page.")
            res.render('index', { title: 'mutation Api' })
        } catch (error) {
            next(error)
        }
    }
}