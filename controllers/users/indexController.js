/* Debug */
const SOURCE = 'mutation:controllers:users:index.js'
const debug = require('debug')(SOURCE)

/* Models */
const UserModel = require('../../models/admin/User')

/* Modules */
const validator = require('../public/ValidatorController')

const { check} = require('express-validator')
module.exports = {
	getAllUsers: async (req, res, next) => {
        try {
            debug("getAllUsers")
            let userRes = await UserModel.find()
                .then(r => {
                    debug(r.length + " founded")
                    return r
                })
                .catch(err=>{
                    throw {msg: "ERROR: "+err.message}
                })
            res.status(200).send(userRes)
        } catch (error) {
            next(error)
        }
    },
    registerUser: async (req, res, next) => {
        try {
            debug("registerUser")
            await UserModel.create({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password,
                token: ''
            })
            .then(async r=>{
                debug('Register success!')
                r.token = await UserModel.generateAuthToken(r)
                res.status(201).send(r)
                return
            })
            .catch(err=>{
                res.status(400).send({msg: "DB: "+err.message})
                return
            })
        } catch (error) {
            next(error)
        }
    },
    registerUserCheckParameters: async (req, res, next) => {
        try {
            debug("checkParametersRegistrations")
            let parametersToCheck = {
                    name:check('name',`name can not be an empty value`).not().isEmpty().run(req),
                    email:check('email',`email can not be an empty value`).not().isEmpty().run(req),
                    email:check('email',`email must be email format`).isEmail().run(req),
                    email:check('email',`email min 6 charateres`).isLength({ min: 6 }).run(req)
                }
            debug('checking parameters')
            const result = await validator(parametersToCheck,req)
            if (!result.isEmpty()) {
                res.status(422).send({msg: result.array()})
                return
            }
            next()
        } catch (error) {
            next(error)
        }
    },
    login: async (req, res, next) => {
        try {
            debug("login")
            const { email, password } = req.body
            const user = await UserModel.findByCredentials(email, password)
            if (!user) {
                res.status(401).send({error: 'Login failed!'})
                return 
            }
            const token = await UserModel.generateAuthToken(user)
            res.send({ user, token })
            return
        } catch (error) {
            next(error)
        }
    },
    loginCheckParameters: async (req, res, next) => {
        try {
            debug("loginCheckParameters")
            let parametersToCheck = {
                    email:check('email',`email must be email format`).isEmail().run(req),
                    password:check('password',`password min  charateres`).isLength({ min:  4}).run(req),
                    password:check('password',`password can not be an empty value`).not().isEmpty().run(req),
                }
            debug('checking parameters')
            const result = await validator(parametersToCheck,req)
            if (!result.isEmpty()) {
                res.status(422).send({msg: result.array()})
                return
            }
            next()
        } catch (error) {
            next(error)
        }
    },
    updateUserCheckParameters: async (req, res, next) => {
        try {
            debug("updateUserCheckParameters")
            let parametersToCheck = {
                    id:check('id',`id can not be an empty value`).not().isEmpty().run(req),
                    id:check('id',`id must be string value`).isString().run(req),
                    name:check('name',`name can not be an empty value`).optional().not().isEmpty().run(req),
                    name:check('name',`email min 6 charateres`).optional().isLength({ min: 6 }).run(req),
                    email:check('email',`email can not be an empty value`).optional().not().isEmpty().run(req),
                    email:check('email',`email must be email format`).optional().isEmail().run(req)
                }
            debug('checking parameters')
            const result = await validator(parametersToCheck,req)
            if (!result.isEmpty()) {
                res.status(422).send({msg: result.array()})
                return
            }
            next()
        } catch (error) {
            next(error)
        }
    },
    updateUser: async (req, res, next) => {
        try {
            debug("Finding user by id: "+req.body.id)
            let user = await UserModel.findOne({_id: req.body.id})
                .then(r=>{
                    debug('User id: '+req.body.id+' finded!')
                    return r
                })
                .catch(err => {
                    res.status(400).send({msg: "DB: "+err.message})
                    return
                })
            if(!user){
                res.status(404).send({msg: 'Can not find resource'})
                return
            }
            debug("updateUser")
            let filter = {_id: req.body.id}
            let setValues = req.body
            let options = {upsert: true}
            await UserModel.updateOne(filter,setValues,options)
            .then(async r=>{
                debug('Updated success!')
                res.status(201).send(r)
                return
            })
            .catch(err=>{
                res.status(400).send({msg: "DB: "+err.message})
                return
            })
        } catch (error) {
            next(error)
        }
    }
}