/* Debug */
const SOURCE = 'mutation:controllers:stats:index.js'
const debug = require('debug')(SOURCE)

/* Models */
const MutationModel = require('../../models/admin/Mutation')

/* Modules */
const validator = require('../public/ValidatorController')


const { check} = require('express-validator')
module.exports = {
	getStats: async (req, res, next) => {
        try {
            debug("getStats")
            let count_mutations = await MutationModel.countDocuments ({hasMutation: true})
                .then(r => {
                    debug("count_mutations: "+ r + " founded")
                    return r
                })
                .catch(err=>{
                    throw {msg: "ERROR: "+err.message}
                })
            let count_no_mutation = await MutationModel.countDocuments({hasMutation: false})
                .then(r => {
                    debug("count_no_mutation: "+ r + " founded")
                    return r
                })
                .catch(err=>{
                    throw {msg: "ERROR: "+err.message}
                })
            const ratio = (count_no_mutation / count_mutations)
            const response = {
                count_mutations,
                count_no_mutation,
                ratio
            }
            return res.status(200).send(response)
        } catch (error) {
            next(error)
        }
    },
    registerDB: async (req, res, next) => {
        try {
            debug("registerDB")
            await MutationModel.create({
                dna: req.body.dna,
                hasMutation: req.body.hasMutation
            })
            .then(async r =>{
                debug('Register success!')
                next()
                return
            })
            .catch(err=>{
                res.status(400).send({msg: "DB: "+err.message})
                return
            })
        } catch (error) {
            next(error)
        }
    },
    response: async (req, res, next) => {
        try {
            debug("response")
            const hasMutation = req.body.hasMutation
            if(hasMutation === true) return res.status(200).send({hasMutation})
            return res.status(403).send({hasMutation})
        } catch (error) {
            next(error)
        }
    },
    validateParams: async (req, res, next) => {
        try {
            debug("validateParams")
            let parametersToCheck = {
                    dna:check('dna',`dna can not be an empty value`).not().isEmpty().run(req),
                    dna:check('dna',`dna need be an array value`).isArray().run(req)
                }
            debug('checking parameters')
            const result = await validator(parametersToCheck,req)
            if (!result.isEmpty()) {
                res.status(422).send({msg: result.array()})
                return
            }
            next()
        } catch (error) {
            next(error)
        }
    },
    verifyDna: async (req, res, next) => {
        try {
            debug("verifyDna")
            const hasMutationRes = await hasMutation(req.body.dna)
            req.body.hasMutation = hasMutationRes
            next()
        } catch (error) {
            next(error)
        }
    }
}
const carateres = [
    "AAAA",
    "TTTT",
    "CCCC",
    "GGGG"
]
const converArrayBidimensional = async (list) => {
    for(let i = 0;i<list.length;i++){
        list[i] = list[i].split('')
    }
    return list
}
const hasMutation = async (dna) => {
    try {
        console.time("time")
        let resHorizontal = await checkHorizontal(dna)
        if(resHorizontal === true) {
            console.timeEnd("time")
            return resHorizontal
        }
        const dnaArray = await converArrayBidimensional(dna)
        let resVertical = await checkVertical(dnaArray)
        if(resVertical === true) {
            console.timeEnd("time")
            return resVertical
        }
        let resOblicuaRight = await checkOblicuaRight(dna)
        if(resOblicuaRight === true) {
            console.timeEnd("time")
            return resOblicuaRight
        }
        let resOblicuaLeft = await checkOblicuaLeft(dna)
        if(resOblicuaLeft === true) {
            console.timeEnd("time")
            return resOblicuaLeft
        }
        console.timeEnd("time")
        return false
    } catch (error) {
        console.error(error,'Error')
    }
    
}
const checkHorizontal = async (array) => {
    for (let x = 0; x < array.length; x++) {
        let status = new RegExp(carateres.join('|')).test(array[0])
        if (status === true ) return true
    }
    return false
}
const checkVertical = async (array) => {
    for (let x = 0; x < array.length; x++) {
        let arrayNew = []
        for (let i = 0; i < array.length; i++) {
            arrayNew.push(array[i][x])
            continue
        }
        const string = arrayNew.join('')
        let status = new RegExp(carateres.join('|')).test(string)
        if (status === true ) return true
    }
    return false
}
const checkOblicuaRight = async (array) => {
    for (let x = 0; x < array.length; x++) {
        let arrayNew = []
        for (let i = 0; i < array.length; i++) {
            arrayNew.push(array[i][x])
            x++
            continue
        }
        const string = arrayNew.join('')
        let status =  new RegExp(carateres.join('|')).test(string)
        if (status === true ) return true
        break
    }
    return false
}
const checkOblicuaLeft = async (array) => {
    for (let x = 0; x < array.length; x++) {
        let arrayNew = []
        for (let i = (array.length -1 ); i >= 0; i--) {
            arrayNew.push(array[i][x])
            x++
            continue
        }
        const string = arrayNew.join('')
        let status =  new RegExp(carateres.join('|')).test(string)
        if (status === true ) return true
        break
    }
    return false
}