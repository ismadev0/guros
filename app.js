/* Debug */
const SOURCE = 'mutation:app.js'
const debug = require('debug')(SOURCE)
/* Modules */
const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const responseTime = require('response-time')

/* Load files routes */
const indexRouter = require('./routes/index')
const routerUsers = require('./routes/users')
const routerMutation = require('./routes/mutation')

const app = express()

app.use(responseTime())

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

//Get method + path
app.use( (req,res,next) => {
  debug(req.method+' '+req.path)
  next()
})

app.use('/', indexRouter)
app.use('/users', routerUsers)
app.use('/mutation', routerMutation)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  debug("catch 404 and forward to error handler")
  next(createError(404))
})

// error handler
app.use( (err, req, res, next) => {
  console.error(err)
  res.status(err.statusCode || 500).json({message: (err.msg != undefined?err.msg:err)})
  return
})

module.exports = app